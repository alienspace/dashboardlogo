import React, { Component } from 'react';
import {
  Col, Image,
} from 'react-bootstrap';
import { IndexLink } from 'react-router';

import { styles } from './Logo.style';
// import logo from './logo.svg';
export class Logo extends Component{
  render(){
    let STYLE = this.props.style ? this.props.style : styles.LogoContainer;
    if(!this.props.src){
      return (
        <Col xs={12} md={12} className={this.props.className}>
          <IndexLink to={{pathname:(!this.props.dashboard) ? '/' : '/dashboard'}}>
            <h1 style={{margin:0}}>
              <span>Nextdash {navigator.onLine ? <i className={'label label-success'} style={{borderRadius:'50%'}}></i> : <i className={'label label-xs label-danger'} style={{borderRadius:'50%'}}></i>}</span>
            </h1>
          </IndexLink>
        </Col>
      );
    }else{
      return (
        <Col xs={12} md={12} className={this.props.className}>
          <figure style={(!this.props.footer) ? STYLE : styles.LogoContainerFooter}>
            <IndexLink to={{pathname:(!this.props.dashboard) ? '/' : '/dashboard'}}><Image src={this.props.src} responsive /></IndexLink>
          </figure>
        </Col>
      );
    }
  }
}
